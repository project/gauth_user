CONTENTS OF THIS FILE
---------------------

  * Summary
  * Requirements
  * Installation
  * Google Configuration
  * Configuration
  * Credits


SUMMARY
-------

This module allows admins to configure Google Api Client accounts
which should be allowed to be authenticated with google for website users.
Websites can then use this authentication to carry other api requests.

This module don't have functionality of its own,
but can help you to manage accounts, authenticate with google
(i.e. get access token) and use this authentication to carry api requests.

This module is extension of Google Api Client module, it allows you to get the
OAuth2 access token from google and save it.


REQUIREMENTS
------------

1. Google Api Client module - You need to download google api php client module
from https://drupal.org/project/google_api_client


INSTALLATION
------------

1. Follow steps at https://www.drupal.org/node/1897420


GOOGLE CONFIGURATION
--------------------
1. Visit https://console.developers.google.com/project
2. Create a new project with appropriate details,
   if you don't have a project created.
3. Under "Dashboard" on left sidebar click on "Use Google API"
   and enable the services which you want to use by clicking the links.
   i.e. Google Analytics, etc
   You can check enabled apis as a separate tab on the page.
4. Click on "Credentials" on the left sidebar.
5. If you have not created a oauth2.0 client id then create it
   with appropriate details i.e.
     Application Type: Web Application,
     Name: Name of the application
     Authorized Redirect uri's: You can copy the uri shown when you create a
     google oauth account in the admin settings.
6. Copy the client id, client secret, api key
   to configuration form of the google api client module.


CONFIGURATION
-------------
1. Configure the Google Api Client module with the creds from google.

2. Configure Google Authentication for User to enable this Google api client
      account for end user authentication.

3. Set permissions to roles who can authenticate this account.

4. Ready to use this account for api access.

5. Set permission to roles to access default UI for authentication or make
   your own UI or just provide links to authenticate.


CREDITS
-------

The idea came up from no module providing google oauth2 authentication
in drupal 7 and now in drupal 8 the module is separated form gauth.

Current Maintainer: Sadashiv Dalvi <dalvisadashiv@gmail.com>
