<?php

namespace Drupal\gauth_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Google Authentication for Users Create Controller.
 *
 * @package Drupal\gauth_user\Controller
 */
class GauthUserCreate extends ControllerBase {

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Callback constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Google API Client.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Callback URL for Google Authentication for Users.
   *
   * @param int $google_api_client
   *   Google Authentication for Users object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   */
  public function createGauthUserAccount($google_api_client, Request $request) {
    $user_account = $this->currentUser();
    $properties = [
      'google_api_account' => $google_api_client,
      'uid' => $user_account->id(),
    ];
    $gauth_user = $this->entityTypeManager->getStorage('gauth_user')->loadByProperties($properties);
    if (empty($gauth_user)) {
      $gauth_user = $this->entityTypeManager->getStorage('gauth_user')->create($properties)->save();
      if ($gauth_user) {
        $this->messenger()->addMessage($this->t('Google Authentication for user account created, now you need to authenticate.'));
      }
      else {
        $this->messenger()->addError($this->t('Google Authentication for user account creation failed.'));
      }
    }
    if ($destination = $request->get('destination')) {
      return new RedirectResponse(Url::fromUserInput($destination)->toString());
    }
    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

}
