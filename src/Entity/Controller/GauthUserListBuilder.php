<?php

namespace Drupal\gauth_user\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for gauth_user entity.
 *
 * @ingroup gauth_user
 */
class GauthUserListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Gauth User implements a Google Api Client account model. These gauth_user accounts are fieldable entities. You can manage the fields on the <a href="@adminlink">Gauth User admin page</a>.', [
        '@adminlink' => \Drupal::urlGenerator()
          ->generateFromRoute('gauth_user.gauth_user_settings'),
      ]),
    ];
    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the snapshot list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header = ['Id', 'Name', 'Services', 'Is Authenticated', 'User'];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\gauth_user\Entity\GauthUser */

    $row = [
      'id' => $entity->getId(),
      'Name' => $entity->getName(),
      'Services' => implode(", ", _google_api_client_google_services_names($entity->getServices())),
      'is_authenticated' => $entity->getAuthenticated() ? $this->t('Yes') : $this->t('No'),
      'User' => $entity->getOwner()->toLink($entity->getOwner()->getAccountName()),
    ];
    return $row + parent::buildRow($entity);
  }

}
