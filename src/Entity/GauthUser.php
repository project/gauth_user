<?php

namespace Drupal\gauth_user\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\google_api_client\Entity\GoogleApiClient;
use Drupal\google_api_client\GoogleApiClientInterface;
use Drupal\user\UserInterface;

/**
 * Defines the GauthUser entity.
 *
 * @ingroup gauth_user
 *
 * This is the main definition of the entity type. From it, an entityType is
 * derived. The most important properties in this example are listed below.
 *
 * id: The unique identifier of this entityType. It follows the pattern
 * 'moduleName_xyz' to avoid naming conflicts.
 *
 * label: Human readable name of the entity type.
 *
 * handlers: Handler classes are used for different tasks. You can use
 * standard handlers provided by D8 or build your own, most probably derived
 * from the standard class. In detail:
 *
 * - view_builder: we use the standard controller to view an instance. It is
 *   called when a route lists an '_entity_view' default for the entityType
 *   (see routing.yml for details. The view can be manipulated by using the
 *   standard drupal tools in the settings.
 *
 * - list_builder: We derive our own list builder class from the
 *   entityListBuilder to control the presentation.
 *   If there is a view available for this entity from the views module, it
 *   overrides the list builder. @todo: any view? naming convention?
 *
 * - form: We derive our own forms to add functionality like additional fields,
 *   redirects etc. These forms are called when the routing list an
 *   '_entity_form' default for the entityType. Depending on the suffix
 *   (.add/.delete) in the route, the correct form is called.
 *
 * - access: Our own accessController where we determine access rights based on
 *   permissions.
 *
 * More properties:
 *
 *  - base_table: Define the name of the table used to store the data. Make sure
 *    it is unique. The schema is automatically determined from the
 *    BaseFieldDefinitions below. The table is automatically created during
 *    installation.
 *
 *  - fieldable: Can additional fields be added to the entity via the GUI?
 *    Analog to content types.
 *
 *  - entity_keys: How to access the fields. Analog to 'nid' or 'uid'.
 *
 *  - links: Provide links to do standard tasks. The
 *    'delete-form' links are added to the list built by the
 *    entityListController. They will show up as action buttons in an additional
 *    column.
 *
 * There are many more properties to be used in an entity type definition. For
 * a complete overview, please refer to the '\Drupal\Core\Entity\EntityType'
 * class definition.
 *
 * The following construct is the actual definition of the entity type which
 * is read and cached. Don't forget to clear cache after changes.
 *
 * @ContentEntityType(
 *   id = "gauth_user",
 *   label = @Translation("Gauth User entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gauth_user\Entity\Controller\GauthUserListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\gauth_user\GauthUserAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\gauth_user\Form\GauthUserForm",
 *       "delete" = "Drupal\gauth_user\Form\GauthUserDeleteForm",
 *       "revoke" = "Drupal\gauth_user\Form\GauthUserRevokeForm",
 *     },
 *   },
 *   base_table = "gauth_user",
 *   admin_permission = "administer google api settings",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid"
 *   },
 *   links = {
 *     "canonical" = "/gauth_user/{gauth_user}",
 *     "delete-form" = "/admin/config/services/gauth_user/{gauth_user}/delete",
 *     "revoke-form" = "/admin/config/services/gauth_user/{gauth_user}/revoke",
 *     "collection" = "/admin/config/services/gauth_user/list"
 *   },
 *   field_ui_base_route = "gauth_user.gauth_user_settings",
 *   constraints = {
 *     "GauthUserOwner" = {}
 *   }
 * )
 *
 * The 'links' above are defined by their path. For core to find the
 * corresponding route, the route name must follow the correct pattern:
 *
 * entity.<entity-name>.<link-name> (replace dashes with underscores)
 * Example: 'entity.gauth.canonical'
 *
 * See routing file above for the corresponding implementation
 *
 * The 'GauthUser' class defines methods and fields for the gauth_user entity.
 *
 * Being derived from the ContentEntityBase class, we can override the methods
 * we want. In our case we want to provide access to the standard fields about
 * creation and changed time stamps.
 *
 * Our interface (see GauthInterface) also exposes the EntityOwnerInterface.
 * This allows us to provide methods for setting and providing ownership
 * information.
 *
 * The most important part is the definitions of the field properties for this
 * entity type. These are of the same type as fields added through the GUI, but
 * they can by changed in code. In the definition we can define if the user with
 * the rights privileges can influence the presentation (view) of each
 * field.
 */
class GauthUser extends ContentEntityBase implements GoogleApiClientInterface {

  /**
   * Google Api Client.
   *
   * @var \Drupal\google_api_client\Entity\GoogleApiClient
   */
  protected $googleApiClient;

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->get('id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->getGoogleApiClient()->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getDeveloperKey() {
    return $this->getGoogleApiClient()->get('developer_key');
  }

  /**
   * {@inheritdoc}
   */
  public function getClientId() {
    return $this->getGoogleApiClient()->get('client_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret() {
    return $this->getGoogleApiClient()->get('client_secret')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getScopes() {
    $scopes = $this->getGoogleApiClient()->get('scopes')->getValue();
    $merged_scopes = [];
    foreach ($scopes as $key => $scope) {
      $merged_scopes[$key] = $scope['value'];
    }
    $services = $this->getGoogleApiClient()->getServices();
    $all_scopes = google_api_client_google_services_scopes($services);
    $return = [];
    foreach ($merged_scopes as $scope) {
      foreach ($all_scopes as $scopes) {
        if (isset($scopes[$scope])) {
          $return[] = $scopes[$scope];
          break;
        }
      }
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getServices() {
    $services = $this->getGoogleApiClient()->get('services')->getValue();
    $return = [];
    foreach ($services as $key => $service) {
      $return[$key] = $service['value'];
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken() {
    return $this->get('access_token')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticated() {
    return $this->get('is_authenticated')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessType() {
    return $this->getGoogleApiClient()->get('access_type')->value;
  }

  /**
   * Function gets Google Api Client object.
   *
   * @return \Drupal\google_api_client\Entity\GoogleApiClient
   *   Returns object of GoogleApiClient.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getGoogleApiClient() {
    if ($this->googleApiClient) {
      return $this->googleApiClient;
    }
    else {
      $id = $this->getGoogleApiAccount();
      $this->googleApiClient = \Drupal::entityTypeManager()->getStorage('google_api_client')->load($id);
      return $this->googleApiClient;
    }
  }

  /**
   * Function returns google api client id.
   *
   * @return int
   *   Returns google api client id.
   */
  public function getGoogleApiAccount() {
    return $this->get('google_api_account')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    return $this->getGoogleApiClient()->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function setDeveloperKey($key) {
    return $this->getGoogleApiClient()->set('developer_key', $key);
  }

  /**
   * {@inheritdoc}
   */
  public function setClientId($client_id) {
    return $this->getGoogleApiClient()->set('client_id', $client_id);
  }

  /**
   * {@inheritdoc}
   */
  public function setClientSecret($secret) {
    return $this->getGoogleApiClient()->set('client_secret', $secret);
  }

  /**
   * {@inheritdoc}
   */
  public function setScopes(array $scopes) {
    $services = $this->getGoogleApiClient()->getServices();
    $all_scopes = google_api_client_google_services_scopes($services);
    $merged_scopes = [];
    foreach ($scopes as $scope) {
      foreach ($all_scopes as $scopes) {
        if (in_array($scope, $scopes)) {
          $merged_scopes[] = array_search($scope, $scopes);
          break;
        }
      }
    }
    return $this->getGoogleApiClient()->set('scopes', $merged_scopes);
  }

  /**
   * {@inheritdoc}
   */
  public function setServices(array $services) {
    return $this->getGoogleApiClient()->set('services', $services);
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessToken($token) {
    if (is_array($token)) {
      $token = (object) $token;
      $token = Json::encode($token);
    }
    return $this->set('access_token', $token);
  }

  /**
   * {@inheritdoc}
   */
  public function setAuthenticated($authentication) {
    return $this->set('is_authenticated', $authentication);
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessType($type) {
    return $this->getGoogleApiClient()->set('access_type', $type);
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * Function to set google api client account.
   */
  public function setGoogleApiAccount($googleApiClient) {
    if ($googleApiClient instanceof GoogleApiClient) {
      $this->set('google_api_account', $googleApiClient->getId());
      $this->googleApiClient = $googleApiClient;
    }
    elseif (is_numeric($googleApiClient)) {
      $this->set('google_api_account', $googleApiClient);
      $this->googleApiClient = \Drupal::entityTypeManager()->getStorage('google_api_client')->load($googleApiClient);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('GoogleApiClient ID'))
      ->setDescription(t('The ID of the Gauth User entity.'))
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The GAuth User UUID.'))
      ->setReadOnly(TRUE);

    $fields['access_token'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Access Token'))
      ->setDescription(t('Access token from google'))
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['is_authenticated'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is Authenticated'))
      ->setDescription(t('Is google_api_client account authenticated'))
      ->setDefaultValue(FALSE)
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    // Owner field of the google_api_client.
    // Entity reference field, holds the reference to the user object.
    // The view shows the user name field of the user.
    // The form presents a auto complete field for the user name.
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['google_api_account'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Google Api Client'))
      ->setDescription(t('The google api client, if this is blank then enable accounts from Gauth user settings.'))
      ->setSetting('target_type', 'google_api_client')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['property' => ['id' => gauth_user_enabled_google_api_client()]])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the uid entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    $values['is_authenticated'] = FALSE;
  }

}
