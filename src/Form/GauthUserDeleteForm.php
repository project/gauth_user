<?php

namespace Drupal\gauth_user\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a form for deleting a gauth_user entity.
 *
 * @ingroup gauth_user
 */
class GauthUserDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this account');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t("This account will be deleted from the system and won't be available");
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t("Delete");
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.gauth_user.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $google_api_client = $this->entity;
    $google_api_client->delete();
    parent::submitForm($form, $form_state);
    $this->messenger()->addMessage($this->t('GoogleApiClient account of the user deleted successfully'));
    if ($destination = \Drupal::request()->get('destination')) {
      $redirect = new RedirectResponse(Url::fromUserInput($destination)->toString());
      $redirect->send();
    }
    $this->redirect('entity.gauth_user.collection')->send();
  }

}
