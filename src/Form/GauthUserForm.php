<?php

namespace Drupal\gauth_user\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form controller for the gauth_user entity edit forms.
 *
 * @ingroup gauth_user
 */
class GauthUserForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\google_api_client\Entity\GoogleApiClient */
    if (empty(gauth_user_enabled_google_api_client())) {
      $this->messenger()->addError($this->t('Please configure atleast one Google Api Client to be used for User Authentication.'));
      return new RedirectResponse(Url::fromRoute('entity.gauth_user.collection')->toString());
    }
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $entity = $this->entity;
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addMessage($this->t('The google_api_client %feed has been updated.', ['%feed' => $entity->toLink()->toString()]));
    }
    else {
      $this->messenger()->addMessage($this->t('The google_api_client %feed has been added. You now need to authenticate this new account.', ['%feed' => $entity->toLink()->toString()]));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $status;
  }

}
