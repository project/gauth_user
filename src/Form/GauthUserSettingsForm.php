<?php

namespace Drupal\gauth_user\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\gauth_user\Form
 *
 * @ingroup gauth_user
 */
class GauthUserSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SiteConfigureForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'gauth_user_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $google_api_clients = array_filter($values['google_api_clients']);
    $google_api_clients = array_keys($google_api_clients);
    $config = $this->configFactory->getEditable('gauth_user.settings');
    $config->set('google_api_clients', $google_api_clients)->save();
    $this->entityTypeManager->clearCachedDefinitions();
    parent::submitForm($form, $form_state);
  }

  /**
   * Define the form used for ContentEntityExample settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $link = Link::createFromRoute('Google Api Client Listing', 'entity.google_api_client.collection')->toString();
    $form['google_api_client_settings']['#markup'] = $this->t('Settings form for Google Authentication for users. Manage field settings here.<br/><br/>');
    $form['google_api_client_intro']['#markup'] = $this->t("Here we can enable which Google Api Client accounts we want to get authenticated from the user. <br/>
                                                   Example if you want to access google calendars of users, then create a Google Api Client account for Google calendar at %google_api_client
                                                   This account will be listed below, enable it here, set permission which role can authenticate it.<br/>", ['%google_api_client' => $link]);
    $google_api_clients = $this->entityTypeManager->getStorage('google_api_client')->loadMultiple();
    $options = [];
    foreach ($google_api_clients as $account) {
      $options[$account->getId()] = $account->getName();
    }
    $enabled_accounts = $this->configFactory->get('gauth_user.settings')->get('google_api_clients');
    $form['google_api_clients'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $enabled_accounts,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gauth_user.settings',
    ];
  }

}
