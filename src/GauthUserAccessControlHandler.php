<?php

namespace Drupal\gauth_user;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the "Media Type" entity type.
 *
 * @see \Drupal\media\Entity\MediaType
 */
class GauthUserAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation === 'authenticate' || $operation == 'revoke' || $operation == 'delete' || $operation == 'create') {
      if ($account->hasPermission('administer gauth user settings')) {
        return AccessResult::allowed();
      }
      elseif ($operation == 'create') {
        $permission = 'authenticate gauth_user ' . $entity->getGoogleApiClient()->getId();
        return AccessResult::allowedIfHasPermission($account, $permission);
      }
      else {
        return AccessResult::allowedIf($entity->getOwnerId() == $account->id());
      }
    }
    else {
      return parent::checkAccess($entity, $operation, $account);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $permission = 'authenticate gauth_user ' . $entity_bundle;
    return AccessResult::allowedIfHasPermission($account, $permission);
  }

}
