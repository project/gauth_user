<?php

namespace Drupal\gauth_user;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Google Authentication for Users Permissions.
 *
 * Each enabled account creates one authentication permission which can be
 * granted to intended roles.
 *
 * @package Drupal\gauth_user\Controller
 */
class GauthUserPermissions {

  use StringTranslationTrait;

  /**
   * Function returns permissions for each account enabled.
   *
   * @return array
   *   Return permission array.
   */
  public function permissions() {
    $permissions = [];
    // Generate permissions for each Google Api Client account.
    $enabled_accounts = \Drupal::configFactory()->get('gauth_user.settings')->get('google_api_clients');
    $google_api_clients = \Drupal::entityTypeManager()->getStorage('google_api_client')->loadMultiple($enabled_accounts);
    foreach ($google_api_clients as $account) {
      if ($permission = $account->getName()) {
        $permissions['authenticate gauth_user ' . $account->getId()] = [
          'title' => $this->t('Authenticate @label account', ['@label' => $permission]),
        ];
      }
    }
    return $permissions;
  }

}
