<?php

namespace Drupal\gauth_user\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Supports validating comment author names.
 *
 * @Constraint(
 *   id = "GauthUserOwner",
 *   label = @Translation("Google Authentication for Users Owner", context = "Validation"),
 *   type = "entity:gauth_user"
 * )
 */
class GauthUserOwnerConstraint extends CompositeConstraintBase {

  /**
   * Message shown when an user is not allowed to create this google account.
   *
   * @var string
   */
  public $messageOwnerNotAllowed = 'This google api client %name is not allowed to be authenticated by %user.';

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return ['google_api_account', 'uid'];
  }

}
