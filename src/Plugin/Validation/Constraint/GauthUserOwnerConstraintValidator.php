<?php

namespace Drupal\gauth_user\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the GauthUserOwner constraint.
 */
class GauthUserOwnerConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Validator 2.5 and upwards compatible execution context.
   *
   * @var \Symfony\Component\Validator\Context\ExecutionContextInterface
   */
  protected $context;

  /**
   * User storage handler.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Google Api Client storage handler.
   *
   * @var \Drupal\google_api_client\GoogleApiClientInterface
   */
  protected $googleApiClientStorage;

  /**
   * Constructs a new GauthUserOwnerConstraintValidator.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage handler.
   * @param \Drupal\Core\Entity\Sql\SqlContentEntityStorage $google_api_client_storage
   *   The user storage handler.
   */
  public function __construct(UserStorageInterface $user_storage, SqlContentEntityStorage $google_api_client_storage) {
    $this->userStorage = $user_storage;
    $this->googleApiClientStorage = $google_api_client_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager')->getStorage('user'),
      $container->get('entity_type.manager')->getStorage('google_api_client'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    $owner_id = (int) $entity->uid->target_id;
    $google_api_client_id = (int) $entity->google_api_account->target_id;
    $google_api_client = $this->googleApiClientStorage->load($google_api_client_id);
    $user = $this->userStorage->load($owner_id);
    $access = $user->hasPermission('authenticate gauth_user ' . $google_api_client->getId());

    if (!$access) {
      $this->context->buildViolation($constraint->messageOwnerNotAllowed, ['%user' => $user->getUsername(), '%name' => $google_api_client->getName()])
        ->atPath('uid')
        ->addViolation();
    }
  }

}
